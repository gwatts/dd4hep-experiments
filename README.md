# DD4hep Experiments

Exploratory code looking for how to use the DD4hep library.

## Introduction

Create a simple planar detector running on docker.

## Use

I see no reason why this wouldn't run under singularity, though I've not tested it yet.

1. Start docker running. Make sure that docker settings allow for the disk to be shared. Use the command
  docker run --rm -i -t -l dd4hep -v local-path-to-this-repro:/dd4hep-experiments whit2333/dd4hep -v local-path-to-g4data:/usr/local/share/Geant4-10.3.2/data
   - --rm means delete the container when you are done running (this will mean you'll have to re-run from scratch). Since config requires downloading some large files, you may not want this option.
   - -i means run interactive.
   - This is a big download - so be patient... and starting it once it is downloaded takes 15 or so seconds on my 8 yr old desktop machine.
   - The local path to g4 data can be empty - the script below will download to it. And will save you the time next time you have to download data.
1. This is a stripped down container. Install things for building:
    apt-get update
    apt-get install wget build-essential cmake libxerces-c-dev libglu1-mesa
1. source /dd4hep-experiments/configure-container.sh
   - This will download some missing data files G4 needs to run
   - This can take upwards of an hour or two for some of the larger files (it is 0.7 GB)
   - This will define some environment variables necessary for running
   - It should copy the files to the g4 directory - which is kept accross containers and has to be downloaded once.
1. create a new fresh directory:
   1. mkdir -p /home/example; cd /home/example
1. cmake -D DD4hep_DIR=$DD4hep_DIR -D CMAKE_INSTALL_PREFIX=$PWD /dd4hep-experiments - this will build the make system
1. to build everything just type "make"
1. To show off the geometry issue the following two commands (after making sure that x-windows is setup correclty):
    dd4hep_add_library_path ${PWD}/lib
    geoDisplay -compact /dd4hep-experiments/compact/football_field.xml
1. To run the simulation:
    dd4hep_add_library_path ${PWD}/lib
    dd_sim /dd4hep-experiments/compact/football_field.xml /dd4hep-experiments/sim/sequences.xml
1. To run with the Geant4 visualization, you can do the following:
    cp /dd4hep-experiments/sim/vis.mac .
    dd_sim /dd4hep-experiments/compact/football_field.xml /dd4hep-experiments/sim/sequences.xml
    Use "run/beamOn 10" to run 10 events
1. chmod a+x bin/run_test_MATHSULAExample.sh - no idea why the cmake stuff isn't doing this automatically.
1. To run the tests, do "make test" (make help to see other options)
