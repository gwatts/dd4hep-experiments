#!/bin/bash
#
# Configure a new dd4 container (from whit2333/dd4hep) to be able to run G4 and dd4.
# Execute this with a "source <filename>" from the container's command prompt to make
# sure your shell is setup correctly.

g4_data=/usr/local/share/Geant4-10.3.2/data

function add_g4_data {
    url=$1
    name=$2
    expname=$3

    if [ ! -d "$g4_data/$2" ]; then
        wget --output-document=download.tar.gz $1
        mkdir -p $g4_data
        tar -xzf download.tar.gz -C $g4_data
        rm download.tar.gz
    fi
    #export $3=$g4_data/$2
}

# Downlaod and define the datasets that G4 uses for materials
add_g4_data http://geant4.cern.ch/support/source/G4ENSDFSTATE.2.1.tar.gz G4ENSDFSTATE2.1 G4ENSDFSTATEDATA
add_g4_data http://geant4.cern.ch/support/source/G4EMLOW.6.50.tar.gz G4EMLOW6.50 G4LEDATA
add_g4_data http://geant4.cern.ch/support/source/G4SAIDDATA.1.1.tar.gz G4SAIDDATA1.1 G4SAIDXSDATA
add_g4_data http://geant4.cern.ch/support/source/G4NEUTRONXS.1.4.tar.gz G4NEUTRONXS1.4 bogusdata
add_g4_data http://geant4.cern.ch/support/source/G4NDL.4.5.tar.gz G4NDL4.5 bogusdata
add_g4_data http://geant4.cern.ch/support/source/G4PhotonEvaporation.4.3.2.tar.gz PhotonEvaporation4.3.2 bogusdata
add_g4_data http://geant4.cern.ch/support/source/G4RadioactiveDecay.5.1.1.tar.gz RadioactiveDecay5.1.1 bogusdata
add_g4_data http://geant4.cern.ch/support/source/G4PII.1.3.tar.gz G4PII1.3 bogusdata
add_g4_data http://geant4.cern.ch/support/source/RealSurface.1.0.tar.gz RealSurface1.0 bogusdata
add_g4_data http://geant4.cern.ch/support/source/G4ABLA.3.0.tar.gz G4ABLA3.0 bogusdata

# Setup dd4hep
source /usr/local/bin/thisdd4hep.sh
