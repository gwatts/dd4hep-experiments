///
/// MCPlane.cpp
///
///  Simple 1d detector plane, with readout, with infinite resolution.
///
/// G. Watts (gwatts@uw.edu)
///

#include "DD4hep/DetFactoryHelper.h"
#include "DDRec/Surface.h"
#include "DDRec/DetectorData.h"

#include <iostream>

using namespace dd4hep;
//using namespace dd4hep::detail;
using namespace dd4hep::rec;
using namespace std;

namespace
{
// Create the G4 detector element
static Ref_t create_element(Detector &description, xml_h e, SensitiveDetector sens)
{
    // Fetch out basic parameters from the XML
    // including the size of the detector we want to build here.
    xml_det_t x_det = e;

    auto name = x_det.nameStr();
    auto id = x_det.id();

    xml_dim_t dim = x_det.dimensions();
    double x_len = dim.dim_x();
    double y_len = dim.dim_y();
    double z_len = dim.dim_z();

    xml_dim_t loc = x_det.position();
    Position location(loc.x(), loc.y(), loc.z());

    // materials
    auto scintilator_material = description.air(); //.material("Air"); // G4_PLASTIC_SC_VINYLTOLUENE

    // Create the overall volumne
    DetElement mcplane(name, id);
    PlacedVolume pv;

    // A really simple box of a scintilator
    // Note TGetBBox, on which Box is based, defines half lenths. XML contains full lengths.
    Box scintilator_solid(x_len/2.0, y_len/2.0, z_len/2.0);
    Volume scintilator_vol(name + "_inner_scintilator", scintilator_solid, scintilator_material);

    // Set up the scintilator as readout
    sens.setType("tracker");
    scintilator_vol.setSensitiveDetector(sens);

    // Place it in the detector, and return it!
    Volume mother = description.pickMotherVolume(mcplane);
    pv = mother.placeVolume(scintilator_vol, location);
    pv.addPhysVolID("system", x_det.id());
    mcplane.setPlacement(pv);

    return mcplane;
#ifdef NOTYET


    // Return the full thing built up.
    auto mother = description.pickMotherVolume(mcplane);
    pv = mother.placeVolume(scintilator_volume);
    ////pv.addPhysVolID("system", id);
    ////mcplane.setPlacement(pv);

    //return nullptr;
    return mcplane;
#endif
}
}

// Declare the factory
DECLARE_DETELEMENT(MCPlane, create_element)